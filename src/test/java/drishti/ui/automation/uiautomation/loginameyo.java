package drishti.ui.automation.uiautomation;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class loginameyo {

	static String driverPath = "C:\\Users\\drishti\\eclipse-workspace\\uiautomation\\src\\main\\java\\chromedriver.exe";
	static String ameyoURL = "https://gsm.ameyo.com:8443/app";
	static String langDropdown = "//input[@type=\"text\" and contains(@class,'select-dropdown')]";
	static String langList = "//ul//li//span[contains(text(),'')]";
	static String userId = "//input[@type=\"text\" and contains(@placeholder,'User')]";
	static String userPassword = "//input[@type=\"password\" and contains(@placeholder,'Password')]";
	static String loginBtn = "//span[contains(text(),'Login')]//ancestor::button";
	static String forceLoginOk = "//span[contains(text(),'Ok')]//ancestor::button[@id=\"automationButton1\"]";

	@Test()
	public void testloginUrl() {
		try {
			System.setProperty("webdriver.chrome.driver", driverPath);

			WebDriver driver = new ChromeDriver();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();

			driver.get(ameyoURL);

			String title = driver.getTitle();
			assertEquals(title, "Ameyo");

			driver.findElement(By.xpath(langDropdown)).click();

			List<WebElement> langlist = driver.findElements(By.xpath(langList));
			Thread.sleep(5000);
			for (WebElement we : langlist) {
				System.out.println(we.getText());
			}
			langlist.get(0).click();
			Thread.sleep(5000);

			driver.findElement(By.xpath(userId)).sendKeys("ron");
			driver.findElement(By.xpath(userPassword)).sendKeys("ron");
			driver.findElement(By.xpath(loginBtn)).click();

			if (driver.findElement(By.xpath(forceLoginOk)).isDisplayed()) {
				Thread.sleep(5000);
				driver.findElement(By.xpath(forceLoginOk)).click();
			}
			Thread.sleep(10000);
			String campaignUrl = "agentConfiguration";
			assertEquals(driver.getCurrentUrl().contains(campaignUrl), true);

			driver.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
