package drishti.ui.automation.uiautomation;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test()
	public void testloginUrl() {
		try {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\drishti\\eclipse-workspace\\uiautomation\\src\\main\\java\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();

			driver.get("https://gsm.ameyo.com:8443/app");

			String title = driver.getTitle();
			assertEquals(title, "Ameyo");

			driver.findElement(By.xpath("//input[@type=\"text\" and contains(@class,'select-dropdown')]")).click();

			List<WebElement> langlist = driver.findElements(By.xpath("//ul//li//span[contains(text(),'')]"));
			Thread.sleep(5000);
			for (WebElement we : langlist) {
				System.out.println(we.getText());
			}
			langlist.get(0).click();
			Thread.sleep(5000);

			driver.findElement(By.xpath("//input[@type=\"text\" and contains(@placeholder,'User')]")).sendKeys("ron");
			driver.findElement(By.xpath("//input[@type=\"password\" and contains(@placeholder,'Password')]"))
					.sendKeys("ron");
			driver.findElement(By.xpath("//span[contains(text(),'Login')]//ancestor::button")).click();

			if (driver
					.findElement(By.xpath("//span[contains(text(),'Ok')]//ancestor::button[@id=\"automationButton1\"]"))
					.isDisplayed()) {
				Thread.sleep(5000);

				driver.findElement(
						By.xpath("//span[contains(text(),'Ok')]//ancestor::button[@id=\"automationButton1\"]")).click();
			}
			Thread.sleep(10000);
			String campaignUrl = "agentConfiguration";
			assertEquals(driver.getCurrentUrl().contains(campaignUrl), true);

			driver.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
